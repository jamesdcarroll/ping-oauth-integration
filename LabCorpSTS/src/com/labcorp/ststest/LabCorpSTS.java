package com.labcorp.ststest;

import static com.pingidentity.sts.clientapi.utils.XPathEngine.textContent;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Element;

import com.pingidentity.sts.clientapi.STSClient;
import com.pingidentity.sts.clientapi.STSClientConfiguration;
import com.pingidentity.sts.clientapi.STSClientException;
import com.pingidentity.sts.clientapi.tokens.wsse.BinaryToken;
import com.pingidentity.sts.clientapi.tokens.wsse.UsernameToken;
import com.pingidentity.sts.clientapi.utils.StringUtils;

public class LabCorpSTS {

	//private static String STS_ENDPOINT = "https://rsa-w2k8-pf610.corp.pingidentity.com:9031/pf/sts.wst";
	private static String STS_ENDPOINT = "https://pingfedad:9031/pf/sts.wst";


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//execUNameTokenSimple();
		//execSMTokenSimple();
		execOAuthSamlBearer();
	}
	
	public static void execUNameTokenSimple()
	{
		Element		stsToken;
		STSClient	stsClient;

		STSClientConfiguration	stsClientConfig = new STSClientConfiguration();
		
		stsClientConfig.setStsEndpoint(STS_ENDPOINT);
		stsClientConfig.setInTokenType(STSClientConfiguration.TokenType.USERNAME);
		stsClientConfig.setOutTokenType(STSClientConfiguration.TokenType.OPENTOKEN);
		stsClientConfig.setIgnoreSSLTrustErrors(true);
		stsClientConfig.setSystemAuthType(STSClientConfiguration.SystemAuthType.NONE);
   	
        StringUtils utils = new StringUtils();

        try {
        	stsClient = new STSClient(stsClientConfig);

            UsernameToken usernameToken = new UsernameToken();
            usernameToken.setUsername("joakes");
        	stsToken = stsClient.issueToken(usernameToken);

            String	strOpenToken = textContent(stsToken);
            System.out.println ("OpenToken: " + strOpenToken);

        } catch (MalformedURLException e) {
            // deal with the exception.  
            // in case of a hardcoded endpoint this never happens
            throw new RuntimeException(e);
        } catch (STSClientException e) {
            // deal with the exception
            throw new RuntimeException(e);
        } catch (IOException e) {
            // deal with the exception
            throw new RuntimeException(e);
		}
	}

	public static void execSMTokenSimple()
	{
		Element		stsToken;
		STSClient	stsClient;

		STSClientConfiguration	stsClientConfig = new STSClientConfiguration();

		stsClientConfig.setStsEndpoint(STS_ENDPOINT);
		//stsClientConfig.setStsEndpoint("https://rsa-w2k8-pf610.corp.pingidentity.com:9031/pf/sts.wst");
		stsClientConfig.setStsEndpoint("https://pingfedad:9031/pf/sts.wst?TokenProcessorId=Uname&TokenGeneratorId=Siteminder");
		//stsClientConfig.setInTokenType(STSClientConfiguration.TokenType.USERNAME);
		//stsClientConfig.setOutTokenType(STSClientConfiguration.TokenType.SITEMINDER);
		stsClientConfig.setIgnoreSSLTrustErrors(true);
		stsClientConfig.setSystemAuthType(STSClientConfiguration.SystemAuthType.NONE);
   	
        StringUtils utils = new StringUtils();

        try {
        	stsClient = new STSClient(stsClientConfig);

            UsernameToken usernameToken = new UsernameToken();
            usernameToken.setUsername("svai");
        	stsToken = stsClient.issueToken(usernameToken);

            String	strToken = textContent(stsToken);
            System.out.println ("Token: " + strToken);

        } catch (MalformedURLException e) {
            // deal with the exception.  
            // in case of a hardcoded endpoint this never happens
            throw new RuntimeException(e);
        } catch (STSClientException e) {
            // deal with the exception
            throw new RuntimeException(e);
        } catch (IOException e) {
            // deal with the exception
            throw new RuntimeException(e);
		}
	}
	
	public static void execOAuthSamlBearer()
	{
		String		strAccessToken = null;
		Element		stsToken;
        StringUtils utils = new StringUtils();
		String		strURLEndpoint;
		
		try {
			strAccessToken = RequestOAuthToken("svai");
			if (strAccessToken != null)
			{
				String encodedToken = new String(Base64.encodeBase64(strAccessToken.getBytes()));	

				STSClientConfiguration stsClientConfig = new STSClientConfiguration();
				
				stsClientConfig.setStsEndpoint("https://pingfedad:9031/pf/sts.wst?TokenProcessorId=OBearer&TokenGeneratorId=Siteminder");
				
				//strURLEndpoint = STS_ENDPOINT + "TokenProcessorId=OBearer&TokenGeneratorId=OTK";
				//stsClientConfig.setStsEndpoint(strURLEndpoint);
	        	stsClientConfig.setIgnoreSSLTrustErrors(true);
	        	stsClientConfig.setSystemAuthType(STSClientConfiguration.SystemAuthType.NONE);
	        	stsClientConfig.setEmbeddedToken(true);

	        	STSClient stsClient = new STSClient(stsClientConfig);
	        	
	            BinaryToken	accessToken = new BinaryToken(encodedToken, "urn:pingidentity.com:oauth2:grant_type:validate_bearer");
	        	stsToken = stsClient.issueToken(accessToken);
	        	
	            String formattedRequest = utils.prettyPrint(stsToken);
	            System.out.println(formattedRequest);

	            String	strOpenToken = textContent(stsToken);
	            System.out.println ("SMToken: " + strOpenToken);
			}
			// Construct data
		} catch (Exception e) {
            e.printStackTrace();
		}
    }

	public static String RequestOAuthToken(String strUsername)
	{
		String	strAccessToken = null;
		
		try {
		    // Construct data
		    String data = URLEncoder.encode("client_id", "UTF-8") + "=" + URLEncoder.encode("cc_client", "UTF-8");
		    data += "&" + URLEncoder.encode("grant_type", "UTF-8") + "=" + URLEncoder.encode("client_credentials", "UTF-8");
		    data += "&" + URLEncoder.encode("client_secret", "UTF-8") + "=" + URLEncoder.encode("2Federate", "UTF-8");
		    data += "&" + URLEncoder.encode("scope", "UTF-8") + "=" + URLEncoder.encode("admin", "UTF-8");

		    // Send data
		    URL url = new URL("http://pingfedad:9030/as/token.oauth2");
		    URLConnection conn = url.openConnection();
		    conn.setDoOutput(true);
		    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		    wr.write(data);
		    wr.flush();

			InputStream is = conn.getInputStream();
			InputStreamReader streamReader = new InputStreamReader(is, "UTF-8");

			JSONParser parser = new JSONParser();
		    Map json = (Map<String, String>)parser.parse(streamReader);

		    System.out.println("==toJSONString()==");
		    System.out.println(JSONValue.toJSONString(json));

			Iterator iter = json.entrySet().iterator();
		    System.out.println("==OAuth Request Token Results==");
		    while(iter.hasNext()){
		      Map.Entry entry = (Map.Entry)iter.next();
		      System.out.println(entry.getKey() + "=>" + entry.getValue());
		    }
		    
		    strAccessToken = (String) json.get("access_token");
            wr.close();
		    streamReader.close();
//		    rd.close();
		} catch (Exception e) {
            e.printStackTrace();
		}
		return strAccessToken;
    }

	
	
}
